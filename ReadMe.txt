Plug and Play CI MVC

Steps:

1. Configure your path in CI config.php (ci_files/application/config/config.php)
 => currently set to $config['base_url'] = 'http://localhost/PROJECTS/LESSONS/Clients/ci_mvc_auto_generator/ci_files/';
2. Run the SQL to create the tables in the DB (Database SQL tables/item_sql_table.sql)
3. Open the web file (ci_mvc_generators) on WAMP/XAMP, paste the columns from DB (e.g item_id, item_isbn, item_title, item_author, item_category).
4. You can then grab code for MVC respectively & paste into our  CI MVC files
5. open in browser with your path to ci_files 'http://localhost/PROJECTS/LESSONS/Clients/ci_mvc_auto_generator/ci_files/Item'

Enjoy!!
