    <?php $this_page = 'model'; ?>
    
    <?php include 'header.php'; ?>
    <div class="container">

        <h1>CI Model Generator [Item_model.php]</h1>
        <p class="lead">All you have to change is your table reference (Its currently preset to the 'items' table)</p>

        <?php include 'columns.php'; ?>
<div class="horizontal-mafx">    
  <div class="horizontal-max-large">
    <textarea id="copy-code" type="text" value="copied" ><?php echo '<?php'."\n"; ?>
defined('BASEPATH') OR exit('No direct script access allowed');

class Item_model extends CI_Model
{

  var $table = 'items';


  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }


public function get_all_items()
{
$this->db->from('items');
$query=$this->db->get();
return $query->result();
}


  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('item_id',$id);
    $query = $this->db->get();

    return $query->row();
  }

  public function item_add($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function item_update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function delete_by_id($id)
  {
    $this->db->where('item_id', $id);
    $this->db->delete($this->table);
  }


}
        
</textarea>  
    
 </div>

      <div class="horizontal-max-small">
        <span class="button-basic"><button class="btn btn-lg btn-primary" data-clipboard-action="copy" data-clipboard-target="#copy-code">Grab code</button></span>
      </div>
</div>

        <?php include 'footer.php'; ?>

