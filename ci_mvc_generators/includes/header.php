
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Auto generate MODELS | VIEWS | CONTROLLERS</title>

    <!-- Bootstrap core CSS -->
    <?php if( $this_page == 'index' ){ ?>
    <link  href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"  rel="stylesheet" href="css/style.css">
    <?php }else{ ?>
    <link  href="../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"  rel="stylesheet" href="../css/style.css">
    <?php } ?>
  </head>

  <body>
