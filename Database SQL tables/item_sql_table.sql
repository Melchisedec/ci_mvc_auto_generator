
/*POSTGRES SYNTAX */

CREATE TABLE items (
item_id integer NOT NULL,
item_isbn integer NOT NULL,
item_title varchar(50) NOT NULL,
item_author varchar(50) NOT NULL,
item_category varchar(50) NOT NULL
) ;


INSERT INTO items (item_id, item_isbn, item_title, item_author, item_category) VALUES
(2, 6565, 'Terminator', 'John Arnorld', 'Action'),
(3, 8934, 'Tuxedo', 'Jetli', 'Action comedy');


/* MYSQL SYNTAX */

CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `item_isbn` int(11) NOT NULL,
  `item_title` varchar(50) NOT NULL,
  `item_author` varchar(50) NOT NULL,
  `item_category` varchar(50) NOT NULL

) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `items` (`item_id`, `item_isbn`, `item_title`, `item_author`, `item_category`) VALUES
(2, 6565, 'Terminator', 'John Arnorld', 'Action'),
(3, 8934, 'Tuxedo', 'Jetli', 'Action comedy');